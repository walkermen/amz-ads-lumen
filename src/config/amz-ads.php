<?php
/**
 * Created by PhpStorm.
 * User: dongyanan
 * Date: 2019/8/6
 * Time: 3:41 PM
 */

return [

    'versionStrings' => [

        "apiVersion"         => "v2",
        "applicationVersion" => "2.0"
    ],

    'endpoints' => [

        "na" => [
            "prod"     => "advertising-api.amazon.com",
            "sandbox"  => "advertising-api-test.amazon.com",
            "tokenUrl" => "api.amazon.com/auth/o2/token"
        ],

        "eu" => [
            "prod"     => "advertising-api-eu.amazon.com",
            "sandbox"  => "advertising-api-test.amazon.com",
            "tokenUrl" => "api.amazon.com/auth/o2/token"
        ],

        "fe" => [
            "prod"     => "advertising-api-fe.amazon.com",
            "sandbox"  => "advertising-api-test.amazon.com",
            "tokenUrl" => "api.amazon.com/auth/o2/token"
        ]
    ],

    'clientNa' => [

        "clientId" => env("CLIENT_ID_NA","") ,
        "clientSecret" => env("CLIENT_SECRET_NA", ""),
        "refreshToken" => env("REFRESH_TOKEN_NA", ""),
        "region" => "na",
        "sandbox" => false,
    ],

    'clientEu' => [

        "clientId" => env("CLIENT_ID_EU","") ,
        "clientSecret" => env("CLIENT_SECRET_EU", ""),
        "refreshToken" => env("REFRESH_TOKEN_EU", ""),
        "region" => "eu",
        "sandbox" => false,
    ],

    'clientFe' => [

        "clientId" => env("CLIENT_ID_FE","") ,
        "clientSecret" => env("CLIENT_SECRET_FE", ""),
        "refreshToken" => env("REFRESH_TOKEN_FE", ""),
        "region" => "fe",
        "sandbox" => false,
    ],
];